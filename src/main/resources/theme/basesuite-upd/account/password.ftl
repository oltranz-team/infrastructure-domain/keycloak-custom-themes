<#import "template.ftl" as layout>
<@layout.mainLayout active='password' bodyClass='password'; section>

    <div class="account-root">
        <div class="row" style="background-color:white;
                                border-radius:10px;">
            <div class="col-md-12">
                <h2>${msg("changePasswordHtmlTitle")}</h2>
            </div>
            <div class="col-md-12 subtitle">
                <span class="subtitle">*${msg("allFieldsRequired")}</span>
            </div>
        </div>
        <div class="form-container">
            <form action="${url.passwordUrl}" class="form-horizontal form-wrapper" style="border:none;" method="post">
                <input type="text" id="username" name="username" value="${(account.username!'')}" autocomplete="username" readonly="readonly" style="display:none;">

                <#if password.passwordSet>
                    <div class="f-width">
                        <div class="inputGroup">
                            <input type="password" class="form-control inputField"
                            id="password" name="password" autofocus autocomplete="current-password" placeholder="Current password">
                        </div>
                    </div>
                </#if>

                <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

                <div class="f-width ">
                    <div class="inputGroup">
                        <input type="password" class="form-control inputField"
                            id="password-new" name="password-new" autocomplete="new-password" placeholder="New password">
                    </div>
                </div>

                <div class="f-width">
                    <div class="inputGroup">
                        <input type="password" class="form-control inputField" 
                            id="password-confirm" name="password-confirm" autocomplete="new-password" placeholder="Confirm password">
                    </div>
                </div>

                <div>
                    <button type="submit"
                            class="${properties.kcButtonClass!} ${properties.kcButtonLargeClass!} buttonSubmit" name="submitAction" value="Save">${msg("doSave")}</button>
                </div>
                   
            </form>
        </div>
    </div>
</@layout.mainLayout>
