<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=false displayWide=false; section>
  <#if section = "header">
    <h1 class="text-center"><img class="${properties.kcHeaderLogoClass!}" src="${url.resourcesPath}/img/logo.png"></h1>
    <h4>${msg("doLogIn")}</h4>
  <#elseif section = "form">
    <div id="kc-form">
      <div id="kc-form-wrapper">
        <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">

          <div class="${properties.kcFormGroupClass!}">
            <div class="${properties.kcFormLabelGroupClass}">
              <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
            </div>
            <input
              tabindex="2"
              id="password"
              class="${properties.kcInputClass!}"
              name="password"
              type="password"
              autocomplete="off"
              placeholder="${msg("enterPassword")}"
            />
          </div>

          <div class="${properties.kcFormGroupClass!} ${properties.kcFormSettingClass!}">
            <div id="kc-form-options">
            </div>

          </div>

          <div id="kc-form-buttons" class="${properties.kcFormGroupClass!}">
            <input tabindex="4" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
          </div>
            <div class="${properties.kcFormOptionsWrapperClass!} text-center">
              <#if realm.resetPasswordAllowed>
                <span><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
              </#if>
            </div>
        </form>
      </div>
    </div>
  </#if>

</@layout.registrationLayout>
