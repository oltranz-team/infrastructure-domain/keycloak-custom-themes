<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
<#if section = "header">
    <h1 class="text-center"><img class="${properties.kcHeaderLogoClass!}" src="${url.resourcesPath}/img/logo.png"></h1>
<#elseif section = "form">
  <#if !realm.loginWithEmailAllowed>
    <#assign label = msg("username")>
    <#assign placeholder = msg("enterUsername")>
  <#elseif !realm.registrationEmailAsUsername>
    <#assign label = msg("usernameOrEmail")>
    <#assign placeholder = msg("enterUsernameOrEmail")>
  <#else>
    <#assign label = msg("email")>
    <#assign placeholder = msg("enterEmail")>
  </#if>
  <div id="kc-form" <#if realm.password && social.providers??>class="${properties.kcContentWrapperClass!}"</#if>>
    <div id="kc-form-wrapper" <#if realm.password && social.providers??>class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}"</#if>>
    <#if realm.password>
        <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcFormLabelGroupClass}">
                  <label for="username" class="${properties.kcLabelClass!}">
                    ${label}
                  </label>
                </div>

                <#if usernameEditDisabled??>
                  <input tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}" type="text" disabled />
                <#else>
                  <input
                    tabindex="1"
                    id="username"
                    class="${properties.kcInputClass!}"
                    name="username"
                    placeholder="${placeholder}"
                    value="${(login.username!'')}"
                    type="text"
                    autofocus
                    autocomplete="off"
                  />
                </#if>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcFormLabelGroupClass} ${properties.kcFormDoubleLabelGroupClass}">
                    <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                </div>
                <input tabindex="2" id="password" class="${properties.kcInputClass!}" placeholder="${msg("enterPassword")}" name="password" type="password" autocomplete="off" />
            </div>

            <div class="${properties.kcFormGroupClass!} ${properties.kcFormSettingClass!}">
                <div id="kc-form-options">
                    <#if realm.rememberMe && !usernameEditDisabled??>
                        <div class="checkbox">
                            <label class=${properties.kcCheckboxLabelClass}>
                                <#if login.rememberMe??>
                                    <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox" checked> ${msg("rememberMe")}
                                <#else>
                                    <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox"> ${msg("rememberMe")}
                                </#if>
                            </label>
                        </div>
                    </#if>
                    </div>

                </div>



                <div id="kc-form-buttons" class="${properties.kcFormGroupClass!}">
                    <input type="hidden" id="id-hidden-input" name="credentialId" <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>
                    <input tabindex="4" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
                </div>

                    <#if realm.resetPasswordAllowed>
                    <div class="text-center">
                        <span><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                    </div>
                    </#if> 
        </form>
    </#if>
    </div>
    <#if realm.password && social.providers??>
        <div id="kc-social-providers" class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!} text-center">
          <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 4>${properties.kcFormSocialAccountDoubleListClass!}</#if>">
                <#list social.providers as p>
                    <li class="${properties.kcFormSocialAccountListLinkClass!}"><a href="${p.loginUrl}" id="zocial-${p.alias}" class="zocial ${p.providerId}"> <span>${p.displayName}</span></a></li>
                </#list>
            </ul>
        </div>
    </#if>
    </div>
<#elseif section = "info" >
    <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
        <div class="text-center" id="kc-registration">
            <span>Don't you have an account? <a tabindex="6" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
        </div>
    </#if>
</#if>
</@layout.registrationLayout>
