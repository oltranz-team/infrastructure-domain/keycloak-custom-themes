<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
  <#if section = "header">
    <h1 class="text-center"><img class="${properties.kcHeaderLogoClass!}" src="${url.resourcesPath}/img/logo.png"></h1>
    <h4>${msg("emailVerifyTitle")}</h4>
    <p class="instruction">
      ${msg("emailVerifyInstruction1")}
    </p>
  <#elseif section = "form">
    <hr />
    <p class="instruction">
      ${msg("emailVerifyInstruction2")} <br /><a href="${url.loginAction}">${msg("doClickHere")}</a> ${msg("emailVerifyInstruction3")}
    </p>
  </#if>
</@layout.registrationLayout>
