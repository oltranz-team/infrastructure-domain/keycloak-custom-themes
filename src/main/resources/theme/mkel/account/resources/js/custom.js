$(function() {
  const yearElement = document.getElementById("current-year");
  if (yearElement) {
    yearElement.innerHTML = new Date().getFullYear()
  }
});