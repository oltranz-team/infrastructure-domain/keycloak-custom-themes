<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>
    
    <div class="account-root">
        <div class="row" style="background-color:white;
                                border-radius:10px;">
            <div class="col-md-12">
                <h2>${msg("editAccountHtmlTitle")}</h2>
            </div>
            <div class="col-md-12 subtitle">
                    <span class="subtitle">*${msg("allFieldsRequired")}</span>
            </div>
        </div>
        <div class="form-container">
            <form action="${url.accountUrl}" class="form-horizontal form-wrapper" style="border:none;" method="post">
                <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

                <#if !realm.registrationEmailAsUsername>
                    <div class="f-width ${messagesPerField.printIfExists('username','has-error')}">
                        <div class="inputGroup">
                            <input type="text" class="form-control inputField" id="username"
                                name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                        </div>
                    </div>
                </#if>

                <div class="f-width ${messagesPerField.printIfExists('email','has-error')}">
                    

                    <div class="inputGroup">
                        <input type="text" class="form-control inputField"
                            id="email" name="email" autofocus value="${(account.email!'')}"/>
                    </div>
                </div>

                <div class="f-width ${messagesPerField.printIfExists('firstName','has-error')}">
                    

                    <div class="inputGroup">
                        <input type="text" class="form-control inputField"
                            id="firstName" name="firstName" value="${(account.firstName!'')}"/>
                    </div>
                </div>

                <div class="f-width ${messagesPerField.printIfExists('lastName','has-error')}">
                   
                    <div class="inputGroup">
                        <input type="text" class="form-control inputField"
                            id="lastName" name="lastName" value="${(account.lastName!'')}"/>
                    </div>
                </div>

                
                <div>
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <div>
                        <button type="submit" 
                            class="${properties.kcButtonClass!} ${properties.kcButtonLargeClass!} buttonSubmit"
                            name="submitAction" value="Save">${msg("doSave")}</button>
                    </div>
                </div>
                     
                  
            </form>
        </div>
                                
    </div>
</@layout.mainLayout>
