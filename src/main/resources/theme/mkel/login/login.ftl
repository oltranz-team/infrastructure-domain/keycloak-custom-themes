<#import "template.ftl" as layout>
<#import "./resources/fonts/mkellogo.png" as mkellogo>
<@layout.registrationLayout displayInfo=false displayMessage=false; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "form">
        <#if realm.password>
             <style>
                    .mainContainer{
                        width:100%;
                        height:100%
                    }
                    .mainDiv{
                        padding-top:5%; 
                        margin-left:25%
                        }
                    .pagelogo{
                        padding-left:25%;
                        margin-bottom:10%;
                    }
                    .inputUserName{
                        height:50px;
                        border-radius:5px;
                        background-color:#EEEEEE;
                    }
                    .buttonLogin{
                        width:100%;
                        background-color:#464DF2;
                        color:white;
                        border-radius:5px;
                        border:none;
                        height:50px;
                        font-weight:bold;
                    }
                    .forgetForm{
                        margin-bottom: 40px;
                        margin-top: 5px;
                    }
                    .imageSize{
                        width:20%;height:20%;
                    }
                    
                    @media screen and (max-width: 1024px){
                       .mainContainer{
                        width:100%;
                        height:100%
                        }
                        .mainDiv{
                            padding-top:30%;
                            margin-left:15%;
                            }
                        .pagelogo{
                            padding-left:40%;
                            margin-bottom:10%;
                        }
                        .imageSize{
                            width:50%;height:50%;
                        }
                        .passwordDiv{
                            width: 100%;
                            margin-bottom:10px;
                        }
                        .submitFormGroup{
                            width: 100%;
                        }
                        .forgetForm{
                            width: 100%;
                            margin-bottom:40px;
                            margin-top: 5px;
                            
                        }
                         
                    }
                    @media screen and (max-width: 920px){
                        .mainContainer{
                            width:100%;
                            height:100%
                        }
                        .mainDiv{
                            padding-top:30%;
                            margin-left:15%;
                            }
                        .pagelogo{
                            padding-left:40%;
                            margin-bottom:10%;
                        }
                        .imageSize{
                            width:50%;height:50%;
                        }
                        .submitFormGroup{
                            width: 100%;
                        }
                        .forgetForm{
                            width: 100%;
                            margin-bottom:40px;
                            
                        }
                    }
                    @media screen and (max-width: 720px){
                        .mainContainer{
                            width:100%;
                            height:100%
                        }
                        .mainDiv{
                            padding-top:10%;
                            margin-left:10%;
                            }
                        .pagelogo{
                            padding-left:40%;
                            margin-bottom:10%;
                        }
                        .imageSize{
                            width:50%;height:50%;
                        }
                        .submitFormGroup{
                            width: 100%;
                        }
                        .forgetForm{
                            width: 100%;
                            margin-bottom:40px;
                            
                        }
                    }
                    @media screen and (max-width: 620px){
                        .mainContainer{
                            width:100%;
                            height:100%
                        }
                        .mainDiv{
                            padding-top:10%;
                            margin-left:16%;
                            }
                        .pagelogo{
                            padding-left:40%;
                            margin-bottom:10%;
                        }
                        .imageSize{
                            width:40%;height:40%;
                        }
                        .submitFormGroup{
                            width: 100%;
                        }
                        .forgetForm{
                            width: 100%;
                            margin-bottom:40px;
                            
                        }
                        .forgetDiv{
                            display: flex; padding-left:40%;
                        }
                        .forgetText{
                            text-decoration:none;color:#C0C0C0;
                        }
                    }
                     @media screen and (max-width: 520px){
                        .mainContainer{
                            width:100%;
                            height:100%
                        }
                        .mainDiv{
                            padding-top:20%;
                            margin-left:5%;
                            }
                        .pagelogo{
                            padding-left:40%;
                            margin-bottom:10%;
                        }
                        .imageSize{
                            width:30%;height:40%;
                        }
                        .submitFormGroup{
                            width: 100%;
                        }
                    }
                
             </style>
            <div class="custom-root root">
                <div id="loginbox" class="login-body">
                    <div class="login-wrapper">
                        <div class="login-panel">
                        <div class="logo f-width">
                            <img src="${url.resourcesPath}/fonts/mkellogo.png"
                            alt="logo" class="imageSize" />
                        </div>

                        <div class="panel-body f-width">
                            <#if message?has_content>
                                <div id="login-alert" class="f-width alert alert-danger col-sm-7">
                                    <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>

                            <form id="kc-form-login" class="${properties.kcFormClass!}" class="f-width" onsubmit="login.disabled = true; return true;" action="${url.loginAction?keep_after('^[^#]*?://.*?[^/]*', 'r')}" method="post">
                                    <div class="userNameField f-width ${properties.kcInputWrapperClass!}">
                                        <#if usernameEditDisabled??>
                                            <input tabindex="1" id="username"
                                             class=" inputUserName inputField ${properties.kcInputClass!}"
                                             name="username" value="${(login.username!'')}" 
                                             type="text" disabled placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>"/>
                                        <#else>
                                            <input tabindex="1" id="username" class="inputUserName inputField ${properties.kcInputClass!}"
                                             name="username" value="${(login.username!'')}" type="text" autofocus autocomplete="off" 
                                             placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>"/>
                                        </#if>
                                    </div>
                                    
                                    <div class="passwordDiv f-width ${properties.kcInputWrapperClass!}">
                                        <input tabindex="2" 
                                            id="password" 
                                            class="inputUserName inputField ${properties.kcInputClass!}" 
                                            name="password" 
                                            type="password" 
                                            autocomplete="off" 
                                            placeholder="${msg("password")}"/>
                                    </div>
                                    <div id="kc-form-buttons " class="f-width submitFormGroup">
                                        <div class="">
                                            <input tabindex="4" class="buttonLogin"
                                            name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
                                            <#if realm.password && social.providers??>
                                                <#list social.providers as p>
                                                    <a href="${p.loginUrl}" id="zocial-${p.alias}" class="btn btn-primary">${msg("doLogIn")} With ${p.displayName}</a>
                                                </#list>
                                            </#if>
                                        </div>
                                    </div>

                                    <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
                                        <div class="forgetForm f-width">
                                                <#if realm.resetPasswordAllowed>
                                                    <span><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                                                </#if>
                                        </div> 
                                        <div class="f-width">
                                                    <p style="color:#808080">Don't you have an account?
                                                        <a tabindex="6" href="${url.registrationUrl}"
                                                            style="text-decoration:none;color:#464DF2;"
                                                            class="registerlink">
                                                            ${msg("doRegister")}
                                                        </a>
                                                    </p>
                                           
                                        </div>
                                    </#if>
                            </form>
                        </div>
                        </div>
                        <div class="custom-footer">
                            <h4 class="f-width">Mkel</h4>
                            <h6 class="f-width">Copyright Ⓒ Mkel <span id="current-year"></span>. All rights reserved</h6>
                            </div>
                    </div>  
                </div>
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>