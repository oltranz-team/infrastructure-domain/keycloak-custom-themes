<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true displayMessage=!messagesPerField.existsError('username'); section>
    <#if section = "header">
        ${msg("emailForgotTitle")}
    <#elseif section = "form">
    <style>
      .mainContainer{
          width:100%;background-color:white;
      }
      .mainbox{
        padding-top:10%;margin-left:25%;
      }
      .imgDiv{
          padding-left:25%;margin-bottom:10%;
      }
      .imageSize{
          width:20%;height:20%;
      }
      .submmitInput{
         background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
          border:none;
      }
      .footer{
          padding-top:30%;color:#808080;
      }
       @media screen and (max-width: 1024px){
            .mainContainer{
                width:100%;background-color:white;
                }
            .mainbox{
                padding-top:40%;margin-left:25%;
            }
            .imgDiv{
                padding-left:34%;margin-bottom:10%;
            }
            .imageSize{
                width:40%;height:40%;
            }
            .submmitInput{
                background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
                border:none;
            }
            .footer{
                padding-top:100%;color:#808080;width:500px;
            }
            .leftDiv{
                width:10%;
            }
            .backLink {
                margin-top: 10px;
            }
       }
       @media screen and (max-width: 920px){
           .mainContainer{
                width:100%;background-color:white;
                }
            .mainbox{
                padding-top:40%;margin-left:25%;
            }
            .imgDiv{
                padding-left:35%;margin-bottom:10%;
            }
            .imageSize{
                width:40%;height:40%;
            }
            .submmitInput{
               background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
                border:none;
            }
            .footer{
                padding-top:100%;color:#808080;width:500px;
            }
            .leftDiv{
                width:10%;
            }
       }
       @media screen and (max-width: 720px){
           .mainContainer{
                width:100%;background-color:white;
                }
            .mainbox{
                padding-top:40%;margin-left:25%;
            }
            .imgDiv{
                padding-left:35%;margin-bottom:10%;
            }
            .imageSize{
                width:20%;height:20%;
            }
            .submmitInput{
                background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
                border:none;
            }
            .footer{
                padding-top:100%;color:#808080;width:500px;
            }
            .leftDiv{
                width:10%;
            }
       }
       @media screen and (max-width: 620px){
             .mainContainer{
                width:100%;background-color:white;
                }
            .mainbox{
                padding-top:50%;margin-left:40%;
            }
            .imgDiv{
                padding-left:25%;margin-bottom:10%;
            }
            .imageSize{
                width:20%;height:20%;
            }
            .submmitInput{
                background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
                border:none;
            }
            .footer{
                padding-top:100%;color:#808080;width:500px;
            }
            
       }
       @media screen and (max-width: 520px){
            .mainContainer{
                width:100%;background-color:white;
                }
            .mainbox{
                padding-top:50%;margin-left:40%;
            }
            .imgDiv{
                padding-left:25%;margin-bottom:10%;
            }
            .imageSize{
                width:20%;height:20%;
            }
            .submmitInput{
                background-color:#464DF2;color:white;border-radius:5px;height:50px;margin-top:10px;
                border:none;
            }
            .footer{
                padding-top:100%;color:#808080;width:500px;
            }
       }
    </style>
    <div class="custom-root root">
        <div id="loginbox" class="login-body">
            <div class="login-wrapper">
                <div class="login-panel">
                        <div class="logo f-width">
                            <img src="${url.resourcesPath}/fonts/mkellogo.png"
                             alt="logo" class="imageSize center-vertically"/>
                    </div>
                    <div class="panel-body f-width" >
                        <#if message?has_content>
                            <div id="login-alert" class="alert alert-danger col-sm-7">
                                <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                            </div>
                        </#if>
                        <form id="kc-reset-password-form" class="${properties.kcFormClass!} f-width" action="${url.loginAction}" method="post">
                            <div class="${properties.kcFormGroupClass!} f-width">
                                <div class="${properties.kcLabelWrapperClass!}">
                                    <div class="userNameWrapper ${properties.kcInputWrapperClass!} f-width">
                                        
                                        <#if auth?has_content && auth.showUsername()>
                                            <input type="text" id="username" name="username" class="inputField ${properties.kcInputClass!}" placeholder="Username" 
                                            autofocus value="${auth.attemptedUsername}" aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"/>
                                        <#else>
                                            <input type="text" id="username" name="username" class="inputField ${properties.kcInputClass!}" 
                                            placeholder="Username" autofocus aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"/>
                                        </#if>

                                        <#if messagesPerField.existsError('username')>
                                            <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                                        ${kcSanitize(messagesPerField.get('username'))?no_esc}
                                            </span>
                                        </#if>
                                    </div>
                                </div>
                                <div class="${properties.kcFormGroupClass!} ${properties.kcFormSettingClass!} f-width">
                                
                                    <input class="submmitInput inputField f-width" type="submit" value="${msg("doSubmit")}"/>
                                
                                <div id="kc-form-options" class="backLink f-width ${properties.kcFormOptionsClass!}">
                                    <div class="${properties.kcFormOptionsWrapperClass!}">
                                        <span><a href="${url.loginUrl}" 
                                        style="text-decoration:none; color:#C0C0C0" class="registerlink">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                                                
                    </div>

                </div>

                </div> 
                                <div class="custom-footer">
                            <h4 class="f-width">Mkel</h4>
                            <h6 class="f-width">Copyright Ⓒ Mkel <span id="current-year"></span>. All rights reserved</h6>
                            </div>
            </div>
        </div>
    </div>
</#if>
</@layout.registrationLayout>
